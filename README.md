# 

The boilerplate uses the python framework flask to run the webinterface and to generate the HTML of the book. To generate the PDF it uses the javascript library pagedjs together with the chromium webbrowser in headless mode.

# Installation

First [download](https://gitlab.constantvzw.org/anais_berck/pagedjs-flask-boilerplate/-/archive/main/pagedjs-flask-boilerplate-main.zip) it on gitlab.

Or clone the repository:

`git clone git@gitlab.constantvzw.org:anais_berck/pagedjs-flask-boilerplate.git`

This boilerplate uses Python 3 and the framework [Flask](https://palletsprojects.com/p/flask/).

Find information on how to [install python here](https://www.python.org/downloads/)

To install the requirements, move to the directory this readme is located in, and then run. Optionally create a virtual environment:

`pip install -r requirements.txt`

Then install [pagedjs-cli](https://gitlab.coko.foundation/pagedjs/pagedjs-cli), with the command:

`npm install -g pagedjs-cli`

Finally edit a copy of `settings.example.py` and save it as `settings.py`.
It is important to set the `PAGEDJS_BINARY_PATH`, you should be able to find it
by running: `whereis pagedjs-cli`.

Now you should be able to start the interface with: `./run.sh`.

# Contents of the boilerplate

- `app.py` the flask webapp, generating the views
- `pagedjs.py` helper functions to run pagedjs-cli from python
- `utils.py` utitilities for generative books
- `templates/index.html` template for the index
- `templates/book.html` template for the book itself
- `static/style.css` css style rules for the index and book (it's possible to split them up, also adjust the templates then)
- `static/pagedjs.interface.css` css style rules provided by paged.js to have a more rich preview in debug mode

# Usage

The boilerplate essentially has two views: `index` and `generate`. The `index` shows the homepage and a link to the  `generate` function. In this example the generated book is downloaded as a PDF.

The template `templates/book.html` is used with styles from `static/style.css` to generate the PDF, adjust them to adjust your book. In `app.py` the function generate loads the template, adjust that function to generate context and / or HTML.

In the [documentation of paged.js](https://pagedjs.org/documentation/) a lot of information on print specific functionality in CSS can be found.

# Deployment

Todo

Note: this boilerplate does not make any effort at rate-limiting.