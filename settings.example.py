PAGEDJS_BINARY_PATH = '' 
# Path to the pagedjs-cli executable
# Find it with `whereis pagedjs-cli`
DEBUG = False 
# When set to true, the application doesn't generate a PDF, 
# but uses the pagedjs-polyfill to show a preview of the document
SITEURL = 'http://localhost:5000'
# The url pagedjs-cli tries to connect to, this url is the
# default of the Flask development server
BASEURL = ''
HTML_TMP_DIR = '/tmp/publishing_house/'